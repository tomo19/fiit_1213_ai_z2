#ifndef AGENERALSEARCHING_HH
#define AGENERALSEARCHING_HH

#include "AL.hh"
#include "State.hh"
#include "Node.hh"
#include "Solution.hh"
#include <cstdint>

class IDataStore;
class GamePlay;

/// Algoritmus vseobecneho hladania

///
/// - destruktor rusi objekt frontu, front uvedeny ako parameter konstruktora (IDataStore *const front) sa nerusi explicitne mimo tejto triedy
///
/// \test
/// - Komplet cela trieda
/// - Algoritmus
///   - memory size integrita (nejde do zapronych cisel, 0 po ukonceni algoritmu)
///   - obmedzenie na hlbku, male hlbky (hlbka 0, 1, ...)
///   - obmedzenie na pamet
///
/// Co bolo otestovane (slabo):
/// - algoritmus, priklad zo skript
///   - do sirky - kontrola po nejaku hlbku cca. 3
///   - do hlbky, ohranicujuca hlbka = 5
///     - pozeralo sa na rozvijanie uzlov, nie aj na spravnost zobrazenia stavu
///       - reprezentacia stavu by mala byt ok, testovana vramci triedy GamePlay
///     - ukonecenie rozvijania pri pam. limite 4 000 B
/// - hladanie riesenia a inverzneho riesenia
///   - zo skript
///   - do hlbky, ohranicujuca hlbka = 5
///     - prva maximalna vetva, spodne uzly
///       - spravna postupnost operatorov riesenia: od korena k uzlu
///       - spravna postupnost operatorov inverzneho riesenia: od uzla ku uzlu, inverzne operatory

class AGSearch
{
private:
    unsigned int id;                ///< id algoritmu, kvoli orientacii v spolocnej hash tabulke
    State sstate;                   ///< zaciatocny stav
    State fstate;                   ///< cielovy stav
    uint16_t maxDepth;              ///< max. hlbka stromu po ktoru sa budu rozvijat nove uzly
    int maxMem;                     ///< max. dovolene mnozstvo pamete (B) alokovanej pre strom hladania, vratane miesta obsedeneho frontom
    int mem;                        ///< aktualne obsadena pamet uzlami algoritmu
    IDataStore *front;              ///< strategia hladania (LIFO - do hlbky, FIFO - do sirky)
    GamePlay *gm;                   ///< instacia GamePlay pre jednu konretnu tuto instanciu triedy AGSearch
    unsigned int nextId;            ///< aktualne najvyssie id uzla, este nepouzite
    unsigned int countf;            ///< pocet rozvitych uzlov (count of flowers)
    unsigned int countp;            ///< pocet spracovanych uzlov (count of process)
    Node *fnode;                    ///< uzol s najdenym cielovym stavom

public:
    AGSearch(unsigned int id, IDataStore *front, uint8_t M, uint8_t N, State sstate, State fstate, uint16_t maxDepth, int maxMem);
    ~AGSearch();
    bool goalTest(State *const s);
    void makeInverseSolution(Solution *const solution);
    void makeSolution(Solution *const solution);
    enum AL::ARetStatus run(unsigned int iterations, enum AL::ARetFlag *const rflag = NULL);
    enum AL::ARetStatus runAll(enum AL::ARetFlag *const rflag = NULL);
    enum AL::ARetStatus step(enum AL::ARetFlag *const rflag = NULL);
};

#endif // AGENERALSEARCHING_HH
