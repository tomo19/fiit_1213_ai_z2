#ifndef LIFO_HH
#define LIFO_HH

#include "IDataStore.hh"
#include "Node.hh"
#include <stack>

// push - hodnota kopirovana do LIFO

class LIFO : public IDataStore
{
private:
    stack<Node *> lifo;

public:
    LIFO();
    bool isEmpty();
    Node *pop();
    void push(Node *node);
};

#endif // LIFO_HH
