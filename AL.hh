#ifndef AL_HH
#define AL_HH

namespace AL
{

enum ARetFlag {
    F_NOT_FIND,
    F_MAX_MEM,
    F_THIS_ALGORITMUS,
    F_OTHER_ALGORITMUS,
    F_MAX_DEPTH,
    F_NONE
};

enum ARetStatus {
    RET_SUCCESS,
    RET_FAILURE,
    RET_CONTINUE
};

enum AType {
    A_BREADTH,
    A_DEPTH,
    A_NULL
};

}

#endif // AL_HH
