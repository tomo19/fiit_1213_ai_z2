#ifndef UTILS_HH
#define UTILS_HH

#include "Solution.hh"
#include "State.hh"
#include "AL.hh"
#include <cstdint>
#include <string>

using namespace std;

struct Arguments
{
    string fileIn;              ///< nazov vstupneho suboru
    uint8_t M;                  ///< pocet riadkov hlavolamu
    uint8_t N;                  ///< pocet stlpcov hlavolamu
    State sstate;               ///< zaciatocny stav
    State fstate;               ///< cielovy stav
    enum AL::AType a1type;
    uint16_t a1maxDepth;
    int a1maxMem;
    unsigned int a1runs;
    enum AL::AType a2type;
    uint16_t a2maxDepth;
    int a2maxMem;
    unsigned int a2runs;
    unsigned int runs;
};

extern Arguments margs;         ///< globalna premenne, argumenty, opis problemu

class Utils
{
public:
    // static char digit2char(int digit);
    static string digit2string(int digit);    
    /// \test
    static bool processArguments(int argc, char *argv[], Arguments *const margs);
    /// \test
    static bool readInputFile(string fileName, State *const sstate, State *const fstate, uint8_t *const M, uint8_t *const N);
    /// \test
    static bool compareState(State *const s1, State *const s2);
    /// \test
    static string solution2str(const Solution *const sol);
    static string state2str(const State *const s);
};

#endif // UTILS_HH
