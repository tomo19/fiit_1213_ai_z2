#ifndef NODE_HH
#define NODE_HH

#include "State.hh"
#include "GamePlay_common.hh"
#include <cstdint>

struct Node
{    
    unsigned int id;                     ///< \warning not all time feature /// \todo DEPLOYMENT: remove, used for testing a runtime output to std::access(cerr...nebude to take easy odstranit! (inicializacia, vytvaranie uzlov s id rataju), neni to iba o vypise
    State state;
    Operator op;
    uint16_t depth;
    uint8_t ref;
    struct Node *parent;
};

#endif // NODE_HH
