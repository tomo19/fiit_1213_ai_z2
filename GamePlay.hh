#ifndef GAMEPLAY_HH
#define GAMEPLAY_HH

// ladiace vypisy jednotlivych metod tridy

// #define DEBUG_GPFLOWER                      // flower()
// #define DEBUG_GPCOMPUTSSIZE                 // computeStateMemSize()
// #define DEBUG_GPAPPOR

#include "FS.hh"
#include "GamePlay_common.hh"
#include "State.hh"
#include <cstdint>
#include <string>

using namespace std;

class Stav;

/// N-hlavolam, model hry

/// Jedna instancia triedy pre jednu hru (jedno hladanie riesenia).
///
/// - model hry MxN hlavolamu
/// - vnutorny aktualny stav
/// - zmena aktualneho stavu aplikovanim operatora
/// - ziskanie aktualneho stavu
///
/// Pouzitie:
/// - hranie hry(aplikovanie riesenia)
/// - rozvitie stavov (pre algoritmy hladania)
///
/// Obmedzenia:
/// - max. celkovy pocet policok: 255 (uint8_t)
///  - hodnota policka: string: retazec akejkolvek dlzky
///  - prazdne policko: emptyBox, implicitne "x", definovana v GamePlay_common.h
/// - min. rozmer hry: aspon 2 policka (1x2), (2x1)
/// - max. rozmer hry: tak aby bol max. pocet policok <= 255, MxN <= 255

class GamePlay
{
private:
    uint8_t M;              ///< pocet riadkov
    uint8_t N;              ///< pocet stlpcov
    uint8_t MN;             ///< celkovy pocet policok
    State as;               ///< aktualny stav
    int stateMemSize;       ///< velkost jedneho stavu [B]

private:
    int computeStateMemSize();
    void copyState(State const * const src, State * const dst);
    uint8_t findEmptyBox();
    void swapBox(uint8_t p1, uint8_t p2);

public:    
    GamePlay(uint8_t M, uint8_t N);
    bool appOpr(enum Operator op);
    void flower(FlowerStates * const f, Operator notOp);
    enum Operator getInvOpr(enum Operator op);
    int getStateMemSize();
    void setState(const State *const s);    
    void toOutFormat();
    string toStr();
    string toStrFormat();
    static string oprToStr(enum Operator op);

/// \name TDD
/// Metody vytvorene iba pre pouzitie s TDD testami. Mozu byt odstranene spolu s TDD v deplyment verzii.
/// \warning
/// - EXPERIMENTAL
/// - neotestovane
/// - neintegritne
/// - used only by TDD
///@{

public:                                             // pomocne metody (EXPERIMENTAL!)
    void test_setState(int n, ...);
    void test_findEmptyBox();
};

///@}

#endif // GAMEPLAY_HH
