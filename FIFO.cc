#include "FIFO.hh"
#include "Node.hh"
#include <cassert>
#include <queue>

#include <iostream>

using namespace std;

FIFO::FIFO()
{
}

bool FIFO::isEmpty()
{
    return this->fifo.empty();
}

Node* FIFO::pop()
{
    assert(this->fifo.empty() == false);

    Node *tmp = NULL;
    tmp = this->fifo.front();
    this->fifo.pop();

    return tmp;
}

void FIFO::push(Node *node)
{
    assert(node != NULL);

    this->fifo.push(node);
}
