#ifndef GAMEPLAY_COMMON_HH
#define GAMEPLAY_COMMON_HH

/// \file

const string emptyBox = "x";    ///< prazdne policko na hracej ploche

/// \enum Operator Operatory

///
/// - operatory aplikovatelne na stav State.
/// - ku kazdemu operatoru existuje jeden inverzny operator

enum Operator {
    OP_UP,              ///< posun prazdneho policka nahor
    OP_DOWN,            ///< posun prazdneho policka nadol
    OP_RIGHT,           ///< posun prazdneho policka vpravo
    OP_LEFT,            ///< posun prazdneho policka vlavo
    OP_ROOT,            ///< riadiaci priznak, znamena ze uzol vznikol bez aplikavania operatora, taky uzol je iba jeden - koren stromu
    OP_NOOP             ///< nepouziva sa, ak sa niekde objavy tato hodnota, nieco je zle
};

#endif // GAMEPLAY_COMMON_HH
