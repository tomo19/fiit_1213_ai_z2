#ifndef FS_HH
#define FS_HH

/// \file
/// - struktury na prenos rozvitych stavov medzi (nazov triedy) "Alogoritmom" a \ref GamePlay
/// \todo DOXYGEN DOC: dopisat realny nazov triedy pre Algoritmus

#include "State.hh"
#include "GamePlay_common.hh"
#include <list>

using namespace std;

/// Jeden rozvity stav

struct FlowerState {
    State s;                ///< novy rozvity stav
    Operator op;            ///< operator, pomocou ktoreho vznikol novy stav z predosleho stavu
};

/// \typedef
typedef list<struct FlowerState> FlowerStates; ///< Zoznam vsetkych rozvitych stavov

#endif // FS_HH
