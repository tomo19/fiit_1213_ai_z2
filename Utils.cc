#include "Utils.hh"
#include "GamePlay.hh"
#include "GamePlay_common.hh"
#include <cassert>
#include <cstring>
#include <cstdint>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <list>
#include <exception>
#include <sstream>

using namespace std;

// =======================================================================================
//                                      GLOBALS
// =======================================================================================

Arguments margs;

// =======================================================================================
//                                Utils CLASS METHODS
// =======================================================================================

/*
char Utils::digit2char(int digit)
{
    switch (digit) {
    case 0:
        return '0';
        break;
    case 1:
        return '1';
        break;
    case 2:
        return '2';
        break;
    case 3:
        return '3';
        break;
    case 4:
        return '4';
        break;
    case 5:
        return '5';
        break;
    case 6:
        return '6';
        break;
    case 7:
        return '7';
        break;
    case 8:
        return '8';
        break;
    case 9:
        return '9';
        break;
    default:
        return '%';
        break;
    }

    return '@';
}
*/


// =======================================================================================
// string Utils::digit2string(int digit)
// =======================================================================================

/// Konvertuje cislo na retazec

string Utils::digit2string(int digit)
{
    ostringstream os;
    string s;

    s.clear();

    os << digit;

    s.append(os.str());

    return s;
}


// =======================================================================================
// bool Utils::processArguments(int argc, char *argv[], Arguments *const margs)
// =======================================================================================

/// Spracuje argumenty programu

/// \warning
/// - progresivna specifikacia metody v progrese pisania programu
/// - neosetruje pocet a spravnost argumentov
/// \retval true argumenty su spracovane
/// \retval false argumenty sa nepodarilo spracovat (najskor zly format)

bool Utils::processArguments(int argc, char *argv[], Arguments *const margs)
{
    if ((argc < 10) || (argc > 11)) {
        cerr << "ERROR: Utils::processArguments(): none argument!" << endl;
        return false;
    }

    margs->fileIn.clear();
    margs->fileIn.append(argv[1]);

    if (strcmp(argv[2], "A_BREADTH") == 0)
        margs->a1type = AL::A_BREADTH;
    if (strcmp(argv[2], "A_DEPTH") == 0)
        margs->a1type = AL::A_DEPTH;
    if (strcmp(argv[2], "A_NULL") == 0)
        margs->a1type = AL::A_NULL;
    if (strcmp(argv[6], "A_BREADTH") == 0)
        margs->a2type = AL::A_BREADTH;
    if (strcmp(argv[6], "A_DEPTH") == 0)
        margs->a2type = AL::A_DEPTH;
    if (strcmp(argv[6], "A_NULL") == 0)
        margs->a2type = AL::A_NULL;

    margs->a1maxDepth = atoi(argv[3]);
    margs->a1maxMem = atoi(argv[4]);
    margs->a1runs = atoi(argv[5]);
    margs->a2maxDepth = atoi(argv[7]);
    margs->a2maxMem = atoi(argv[8]);
    margs->a2runs = atoi(argv[9]);

    if (argc == 11)
        margs->runs = atoi(argv[10]);
    else
        margs->runs = 999999999;

    return true;
}


// =======================================================================================
// bool Utils::readInputFile(string fileName, State *const sstate, State *const fstate, uint8_t *const M, uint8_t *const N)
// =======================================================================================

/// Nacitanie vstupneho suboru

/// \todo DOXYGEN DOC: zdokumentovat parametre
/// \note osetruje chyby pri alokacii pamete pri volani .resize(), ale nie v pripade volania .clear() metod na std containery
/// \warning neosetruje pouzivatelsky vstup!
///
/// \retval true subor sa podarilo nacitat
/// \retval false subor sa nepodarilo nacitat

bool Utils::readInputFile(string fileName, State *const sstate, State *const fstate, uint8_t *const M, uint8_t *const N)
{
    // osetrenie argumentov
    assert(sstate != NULL);
    assert(fstate != NULL);
    assert(N != NULL);
    assert(M != NULL);
    assert(fileName.empty() != true);

    uint8_t i_M = 0;
    uint8_t i_N = 0;
    State i_sstate;
    State i_fstate;

    int tmp;
    string stmp;

    // dodatocna inicializacia premennych (vratane argumentov)
    i_sstate.clear();
    i_fstate.clear();

    sstate->clear();
    fstate->clear();
    *M = 0;
    *N = 0;

    // nacitanie vstupu zo suboru
    ifstream f;
    f.open(fileName, ios::in);
    if (f.is_open() == true) {

        // nacitanie rozmerov mriezky
        f >> tmp;
        i_M = (uint8_t) tmp;
        f >> tmp;
        i_N = (uint8_t) tmp;

        // nastavenie velkosti vektorov zaciatocneho a cieloveho stavu
        try {
            i_sstate.resize(i_M * i_N);
            i_fstate.resize(i_M * i_N);
        }
        catch (bad_alloc e) {
            cerr << "ERROR: Utils::readInputFile(): i_[fs]state.resize() allocation problem" << endl;
            cerr << "ERROR: Utils::readInputFile(): e.what(): " << e.what() << endl;
            f.close();
            return false;
        }

        // nacitanie zaciatocneho stavu
        for (uint8_t i = 0; i < i_M * i_N; i++) {
            stmp.clear();
            f >> stmp;
            i_sstate.at(i).clear();
            i_sstate.at(i) = stmp;
        }

        // nacitanie cieloveho stavu
        for (uint8_t i = 0; i < i_M * i_N; i++) {
            stmp.clear();
            f >> stmp;
            i_fstate.at(i).clear();
            i_fstate.at(i) = stmp;
        }
    }
    else {
        cerr << "ERROR: Utils::readInputFile(): unable to open file: " << fileName << endl;
        return false;
    }

    // zatvorenie vstupneho suboru
    f.close();

    // kopirovanie nacitanych hodnot do entit na adresach argumentov

    // rozmery mriezky
    *M = i_M;
    *N = i_N;

    // zaciatocny a cielovy stav

    try {
        sstate->resize(i_M * i_N);
        fstate->resize(i_M * i_N);
    }
    catch (bad_alloc e) {
        cerr << "ERROR: Utils::readInputFile(): i_[fs]state.resize() allocation problem" << endl;
        cerr << "ERROR: Utils::readInputFile(): e.what(): " << e.what() << endl;
        return false;
    }

    for (uint8_t i = 0; i < i_sstate.size(); i++) {
        sstate->at(i) = i_sstate.at(i);
    }
    for (uint8_t i = 0; i < i_fstate.size(); i++) {
        fstate->at(i) = i_fstate.at(i);
    }

    return true;
}


// =======================================================================================
// bool Utils::compareState(State *const s1, State *const s2)
// =======================================================================================

/// Porovnanie dvoch stavov

/// \param s1 prvy porovnavany stav
/// \param s2 druhy porovnavany stav
/// \pre stavy maju rovnaky pocet poloziek, su konzistentne
/// \retval true stavy su rovnake
/// \retval false stavy su odlisne

bool Utils::compareState(State *const s1, State *const s2)
{
    assert((s1 != NULL) && (s2 != NULL));
    // pre istotu
    assert(s1->size() == s2->size());

    // porovnaj
    for (unsigned int i = 0; i < s1->size(); i++) {
        if (s1->at(i) != s2->at(i))
            return false;
    }

    return true;
}


// =======================================================================================
// static string solution2str(const Solution *const sol)
// =======================================================================================

string Utils::solution2str(const Solution *const sol)
{
    string s;
    s.clear();

    s.append("{");
    s.append(Utils::state2str(&sol->sstate));
    s.append("}");

    s.append(": {");

    list<Operator>::const_iterator it;

    for (it = sol->ops.begin(); it != sol->ops.end(); it++) {
        s.append(GamePlay::oprToStr(*it));
        s.append("; ");
    }

    s.append("}");

    return s;
}


// =======================================================================================
// string Utils::state2Str(const State *const s)
// =======================================================================================

/// Stav ako retazec (postupnost policok zlava hore az doprava dole, v smere zlava doprava, v jednom riadku

string Utils::state2str(const State *const s)
{
    assert(s != NULL);

    if (s->size() == 0)
        cerr << "ERROR: Utils::state2Str(): state len = 0!" << endl;

    string str;
    str.clear();

    for (uint8_t i = 0; i < s->size(); i++) {
        str.append(s->at(i));
        str.append(" ");
    }

    return str;
}
