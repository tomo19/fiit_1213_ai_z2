#include "AUtils.hh"
#include "GamePlay.hh"
#include "GamePlay_common.hh"
#include "State.hh"
#include "Utils.hh"
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

// stav - 1 2 3 4 5 6 7 8 m
/// \warning CRAP

Node *AUtils::test_createNode(uint8_t id, uint16_t depth, uint8_t ref, Node *parent)
{
    Node *tmp = NULL;

    tmp = new(nothrow) Node;
    if (tmp == NULL)
        return tmp;

    tmp->state.resize(9);
    for (int i = 0; i <= 7; i++)
        tmp->state.at(i) = Utils::digit2string(i+1);
    tmp->state.at(8) = "m";

    tmp->id = id;
    tmp->depth = depth;
    tmp->ref = ref;
    tmp->parent = parent;

    return tmp;
}


/// \retval Node novy vytvoreny (alokovany v pameti) uzol
/// \retval Null uzol sa nepodarilo vytvorit (alkovat v pameti)

Node *AUtils::createNode(uint8_t id, uint16_t depth, uint8_t ref, Node *parent, State *const s, Operator op)
{
    assert(s->size() != 0);
    assert(op != OP_NOOP);

    Node *tmp = NULL;

    // vytvor novy uzol
    tmp = new(nothrow) Node;
    if (tmp == NULL)
        return tmp;

    // nastav hodnoty uzla
    tmp->id = id;
    tmp->depth = depth;
    tmp->ref = ref;
    tmp->parent = parent;
    tmp->op = op;

    // skopiruj novy stav uzla

    tmp->state.resize(s->size());
    for (uint8_t i = 0; i < s->size(); i++)
        tmp->state.at(i) = s->at(i);

    return tmp;
}


string AUtils::node2str(const Node *const n)
{
    assert(n != NULL);

    ostringstream os;

    os << "[" << (unsigned int) n->id << "](h=" << (unsigned int) n->depth << "); " << GamePlay::oprToStr(n->op) << "; ref=" << (unsigned int) n->ref;

    if (n->parent != NULL)
         os << "; parent.id={" << n->parent->id << "}";
    else
        os << "; parent=NULL";

    os << "; {";
    //for (unsigned int i = 0; i < n->state.size(); i++)
    //     os << n->state.at(i) << " ";
    os << Utils::state2str(&n->state);
    os << "}";

    return os.str();
}

void AUtils::node2out(const Node *const n)
{
    assert(n != NULL);

    cout << AUtils::node2str(n) << endl;
}
