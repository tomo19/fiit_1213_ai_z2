#include "AL.hh"
#include "ABSearch.hh"
#include "AGSearching.hh"
#include "IDataStore.hh"
#include "FIFO.hh"
#include "LIFO.hh"
#include "State.hh"
#include "Utils.hh"
#include <cstdint>
#include <iostream>

using namespace std;

ABSearch::ABSearch(uint8_t M, uint8_t N, State sstate, State fstate, AL::AType a1type, uint16_t a1maxDepth, int a1maxMem, unsigned int a1runs, AL::AType a2type, uint16_t a2maxDepth, int a2maxMem, unsigned int a2runs)
{
    this->a1type = AL::A_NULL;
    this->a1ret = AL::RET_FAILURE;
    this->a1maxDepth = 0;
    this->a1maxMem = 0;
    this->a1runs = 0;
    this->a2type = AL::A_NULL;
    this->a2ret = AL::RET_FAILURE;
    this->a2maxDepth = 0;
    this->a2maxMem = 0;
    this->a2runs = 0;

    this->a1 = NULL;
    this->a2 = NULL;
    this->f1 = NULL;
    this->f2 = NULL;

    this->countf = 0;
    this->countp = 0;

    this->sstate = sstate;
    this->fstate = fstate;

    this->a1type = a1type;
    this->a1maxDepth = a1maxDepth;
    this->a1maxMem = a1maxMem;
    this->a1runs = a1runs;

    this->a2type = a2type;
    this->a2maxDepth = a2maxDepth;
    this->a2maxMem = a2maxMem;
    this->a2runs = a2runs;

    /// \todo OSETRIT: bad_alloc

    // vytvorenie frontov podla typu algoritmu

    if (this->a1type == AL::A_BREADTH)
        this->f1 = new FIFO();
    if (this->a1type == AL::A_DEPTH)
        this->f1 = new LIFO();
    if (this->a1type == AL::A_NULL)
        this->f1 = NULL;

    if (this->a2type == AL::A_BREADTH)
        this->f2 = new FIFO();
    if (this->a2type == AL::A_DEPTH)
        this->f2 = new LIFO();
    if (this->a2type == AL::A_NULL)
        this->f2 = NULL;

    // vytvorenie instancii algoritmov

    if (this->a1type != AL::A_NULL) {
        this->a1 = new AGSearch(1, this->f1, M, N, this->sstate, this->fstate, this->a1maxDepth, this->a1maxMem);
    }

    if (this->a2type != AL::A_NULL) {
        this->a2 = new AGSearch(2, this->f2, M, N, this->sstate, this->fstate, this->a2maxDepth, this->a2maxMem);
    }

    this->a1ret = this->a2ret = AL::RET_CONTINUE;

}

/// \todo

ABSearch::~ABSearch()
{

}

/// \test
/// \todo zrychlit (zlozita jedna podmienka namiesto 100x vnorenych if)

enum AL::ARetStatus ABSearch::step(enum AL::ARetFlag *const rflag)
{
    bool haveSuccess = false;

    if (this->a1type != AL::A_NULL) {
        if (this->a1ret != AL::RET_FAILURE) {
            this->a1ret = this->a1->run(this->a1runs);
            if (this->a1ret == AL::RET_SUCCESS)
                haveSuccess = true;
        }
    }

    if (haveSuccess == false) {
        if (this->a2type != AL::A_NULL) {
            if (this->a2ret != AL::RET_FAILURE) {
                this->a2ret = this->a2->run(this->a2runs);
                if (this->a2ret == AL::RET_SUCCESS)
                    haveSuccess = true;
            }
        }
    }

    if (this->a1ret == AL::RET_SUCCESS) {
        cerr << "ABSearch::step(): MAME RIESENIE !!!" << endl;
        Solution s;
        a1->makeSolution(&s);
        cerr << "===========================================================" << endl;
        cerr << "RIESENIE" << endl;
        cerr << "===========================================================" << endl;
        cerr << "CIELOVY STAV: " << Utils::state2str(&this->fstate) << endl;
        cerr << "SOLUTION: " << Utils::solution2str(&s) << endl;
        cerr << "===========================================================" << endl;
        return AL::RET_SUCCESS;
    }

    if (this->a2ret == AL::RET_SUCCESS) {
        cerr << "ABSearch::step(): MAME RIESENIE !!!" << endl;
        Solution s;
        a2->makeSolution(&s);
        cerr << "===========================================================" << endl;
        cerr << "RIESENIE" << endl;
        cerr << "===========================================================" << endl;
        cerr << "CIELOVY STAV: " << Utils::state2str(&this->fstate) << endl;
        cerr << "SOLUTION: " << Utils::solution2str(&s) << endl;
        cerr << "===========================================================" << endl;
        return AL::RET_SUCCESS;
    }

    if ((this->a1ret == AL::RET_FAILURE) && (this->a2ret == AL::RET_FAILURE)) {
        cerr << "===========================================================" << endl;
        cerr << "RIESENIE SA NENASLO" << endl;
        cerr << "===========================================================" << endl;
        return AL::RET_FAILURE;
    }

    return AL::RET_CONTINUE;
}
