#ifndef AUTILS_HH
#define AUTILS_HH

#include "Node.hh"
#include "GamePlay_common.hh"
#include <string>

class AUtils
{
public:
    static Node *createNode(uint8_t id, uint16_t depth, uint8_t ref, Node *parent, State *const s, Operator op);
    static string node2str(const Node * const n);
    static void node2out(const Node * const n);
    static Node *test_createNode(uint8_t id, uint16_t depth, uint8_t ref, Node *parent);
};

#endif // AUTILS_HH
