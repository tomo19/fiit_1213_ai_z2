#ifndef DATASTORE_HH
#define DATASTORE_HH

#include "Node.hh"

class IDataStore
{
public:    
    virtual bool isEmpty() = 0;
    virtual Node *pop() = 0;
    virtual void push(Node *node) = 0;    
};

#endif // DATASTORE_HH
