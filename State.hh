#ifndef STATE_HH
#define STATE_HH

#include <cstdint>
#include <string>
#include <vector>

using namespace std;

/// \file
/// \todo ARCHITECTURE: State ako trieda s metodami - zvazit
/// - (operacie modifikujuce triedu State rozhadzane momentalne )
/// - crta sa problem vo FlowerStates, FlowerState pri vypise(formatovanom) obsahu stavov
///   - vypis zatial iba v TDD testoch, ako docasna EXPERIMANTAL metoda urcena iba pre testovanie a testovacie vystupy

/// \typedef
typedef vector<string> State; ///< Stav

#endif // STATE_HH
