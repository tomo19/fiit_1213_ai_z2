#include "TDD.hh"
#include "AUtils.hh"
#include "FIFO.hh"
#include "FS.hh"
#include "GamePlay.hh"
#include "GamePlay_common.hh"
#include "LIFO.hh"
#include "Node.hh"
#include "Utils.hh"
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <list>

using namespace std;

// =======================================================================================
//                                    PUBLIC
// =======================================================================================

// =======================================================================================
// void TDD::testAll()
// =======================================================================================

void TDD::testAll()
{
    TDD::testLIFO();
    TDD::testFIFO();
    TDD::testGPConstructorAndOutputs();
    TDD::testGPemptyBox();
    TDD::testGPops();
    TDD::testGPsetStav();
    TDD::testGPswap();
    TDD::testGPflower();
    TDD::testGPgetSize();
}


// =======================================================================================
// =======================================================================================
//                               IDataStore, FIFO, LIFO
// =======================================================================================
// =======================================================================================

// =======================================================================================
// void TDD::testFIFO()
// =======================================================================================

void TDD::testFIFO()
{
    TDD::prtHeader("TDD::testFIFO");

    Node *root = AUtils::test_createNode(0, 0, 0, NULL);
    Node *n1 = AUtils::test_createNode(1, 1, 1, root);
    Node *n2 = AUtils::test_createNode(2, 2, 2, root);
    Node *n3 = AUtils::test_createNode(3, 3, 3, root);

    FIFO fifo;

    if (fifo.isEmpty() == false) {
        cout << "testFIFO(): FIFO is not empty after creation!" << endl;
        abort();
    }

    cout << "--" << endl;
    cout << "testFIFO(): 1 node" << endl;
    cout << "testFIFO(): PUSH: " << AUtils::node2str(n1) << endl;
    fifo.push(n1); assert(fifo.isEmpty() == false);
    cout << "testFIFO(): POP:  " << AUtils::node2str(fifo.pop()) << endl;

    if (fifo.isEmpty() == false) {
        cout << "testFIFO(): FIFO is not empty!" << endl;
        abort();
    }

    cout << "--" << endl;
    cout << "testFIFO(): 2 nodes" << endl;
    cout << "testFIFO(): PUSH: " << AUtils::node2str(n1) << endl;
    cout << "testFIFO(): PUSH: " << AUtils::node2str(n2) << endl;
    fifo.push(n1); assert(fifo.isEmpty() == false);
    fifo.push(n2); assert(fifo.isEmpty() == false);
    cout << "testFIFO(): POP:  " << AUtils::node2str(fifo.pop()) << endl;
    cout << "testFIFO(): POP:  " << AUtils::node2str(fifo.pop()) << endl;

    if (fifo.isEmpty() == false) {
        cout << "testFIFO(): FIFO is not empty!" << endl;
        abort();
    }

    cout << "--" << endl;
    cout << "testFIFO(): 3 nodes" << endl;
    cout << "testFIFO(): PUSH: " << AUtils::node2str(n1) << endl;
    cout << "testFIFO(): PUSH: " << AUtils::node2str(n2) << endl;
    cout << "testFIFO(): PUSH: " << AUtils::node2str(n3) << endl;
    fifo.push(n1); assert(fifo.isEmpty() == false);
    fifo.push(n2); assert(fifo.isEmpty() == false);
    fifo.push(n3); assert(fifo.isEmpty() == false);
    cout << "testFIFO(): POP: " << AUtils::node2str(fifo.pop()) << endl;
    cout << "testFIFO(): POP: " << AUtils::node2str(fifo.pop()) << endl;
    cout << "testFIFO(): POP: " << AUtils::node2str(fifo.pop()) << endl;

    if (fifo.isEmpty() == false) {
        cout << "testFIFO(): FIFO is not empty!" << endl;
        abort();
    }

    // PASS: assert() cause abort()
    /*
    cout << "--" << endl;
    cout << "testFIFO(): pop() from empty FIFO" << endl;
    fifo.pop();
    */

    // PASS
    /*
    cout << "--" << endl;
    cout << "testFIFO(): push() NULL" << endl;
    fifo.push(NULL);
    */

    delete root;
    delete n1;
    delete n2;
    delete n3;
}


// =======================================================================================
// void TDD::testLIFO()
// =======================================================================================

void TDD::testLIFO()
{
    TDD::prtHeader("TDD::testLIFO()");

    Node *root = AUtils::test_createNode(0, 0, 0, NULL);
    Node *n1 = AUtils::test_createNode(1, 1, 1, root);
    Node *n2 = AUtils::test_createNode(2, 2, 2, root);
    Node *n3 = AUtils::test_createNode(3, 3, 3, root);

    LIFO lifo;

    if (lifo.isEmpty() == false) {
        cout << "testLIFO(): LIFO is not empty after creation!" << endl;
        abort();
    }

    cout << "--" << endl;
    cout << "testLIFO(): 1 node" << endl;
    cout << "testLIFO(): PUSH: " << AUtils::node2str(n1) << endl;
    lifo.push(n1); assert(lifo.isEmpty() == false);
    cout << "testLIFO(): POP:  " << AUtils::node2str(lifo.pop()) << endl;

    if (lifo.isEmpty() == false) {
        cout << "testLIFO(): LIFO is not empty!" << endl;
        abort();
    }

    cout << "--" << endl;
    cout << "testLIFO(): 2 nodes" << endl;
    cout << "testLIFO(): PUSH: " << AUtils::node2str(n1) << endl;
    cout << "testLIFO(): PUSH: " << AUtils::node2str(n2) << endl;
    lifo.push(n1); assert(lifo.isEmpty() == false);
    lifo.push(n2); assert(lifo.isEmpty() == false);
    cout << "testLIFO(): POP:  " << AUtils::node2str(lifo.pop()) << endl;
    cout << "testLIFO(): POP:  " << AUtils::node2str(lifo.pop()) << endl;

    if (lifo.isEmpty() == false) {
        cout << "testLIFO(): LIFO is not empty!" << endl;
        abort();
    }

    cout << "--" << endl;
    cout << "testLIFO(): 3 nodes" << endl;
    cout << "testLIFO(): PUSH: " << AUtils::node2str(n1) << endl;
    cout << "testLIFO(): PUSH: " << AUtils::node2str(n2) << endl;
    cout << "testLIFO(): PUSH: " << AUtils::node2str(n3) << endl;
    lifo.push(n1); assert(lifo.isEmpty() == false);
    lifo.push(n2); assert(lifo.isEmpty() == false);
    lifo.push(n3); assert(lifo.isEmpty() == false);
    cout << "testLIFO(): POP:  " << AUtils::node2str(lifo.pop()) << endl;
    cout << "testLIFO(): POP:  " << AUtils::node2str(lifo.pop()) << endl;
    cout << "testLIFO(): POP:  " << AUtils::node2str(lifo.pop()) << endl;

    if (lifo.isEmpty() == false) {
        cout << "testLIFO(): LIFO is not empty!" << endl;
        abort();
    }

    // PASS: assert() cause abort()
    /*
    cout << "--" << endl;
    cout << "testLIFO(): pop() from empty LIFO" << endl;
    lifo.pop();
    */

    // PASS: assert() cause abort()
    /*
    cout << "--" << endl;
    cout << "testLifo(): push(): push NULL" << endl;
    lifo.push(NULL);
    */

    delete root;
    delete n1;
    delete n2;
    delete n3;
}


// =======================================================================================
// =======================================================================================
//                                     GamePlay
// =======================================================================================
// =======================================================================================

// =======================================================================================
// void TDD::testGPConstructorAndOutputs()
// =======================================================================================

void TDD::testGPConstructorAndOutputs()
{
    TDD::prtHeader("TDD::testGPConstructorAndOutputs()");

    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 1x2" << endl;
    GamePlay g2(1, 2);
    g2.toOutFormat();
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 2x1" << endl;
    GamePlay g3(2, 1);
    g3.toOutFormat();
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 2x2" << endl;
    GamePlay g4(2, 2);
    g4.toOutFormat();

    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 1x3" << endl;
    GamePlay g5(1, 3);
    g5.toOutFormat();
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 3x1" << endl;
    GamePlay g6(3, 1);
    g6.toOutFormat();
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 2x3" << endl;
    GamePlay g8(2, 3);
    g8.toOutFormat();
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 3x2" << endl;
    GamePlay g9(3, 2);
    g9.toOutFormat();
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 3x3" << endl;
    GamePlay g7(3, 3);
    g7.toOutFormat();

    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 51x5" << endl;
    GamePlay g10(51, 5);
    g10.toOutFormat();

    // PASS
    /*
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 0x0" << endl;
    GamePlay g11(0, 0);
    g11.toOutFormat();
    */

    // PASS
    /*
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 0x1" << endl;
    GamePlay g12(0, 1);
    g12.toOutFormat();
    */

    // PASS
    /*
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 1x0" << endl;
    GamePlay g13(1, 0);
    g13.toOutFormat();
    */

    // PASS
    /*
    cout << "--" << endl;
    cout << "TDD::testGPConstructorAndOutputs(): 0x0: 1x1" << endl;
    GamePlay g1(1, 1);
    g1.toOutFormat();
    */
}


// =======================================================================================
// static void TDD::testGPemptyBox()
// =======================================================================================

void TDD::testGPemptyBox()
{
    TDD::prtHeader("static void TDD::testGPemptyBox()");

    GamePlay g(1, 2);
    g.test_findEmptyBox();
}


// =======================================================================================
// void TDD::testGPflower()
// =======================================================================================

void TDD::testGPflower()
{
    TDD::prtHeader("void TDD::testGPflower()");

    FlowerStates fs;
    fs.clear();

    cout << "--" << endl;
    cout << "Testovanie zachovania povodneho stavu po rozviti uzla" << endl;
    cout << "-----------------------------------------------------" << endl;

    GamePlay g1(3, 3);
    g1.test_setState(9, "1", "2", "3", "4", "x", "5", "6", "7", "8");

    cout << "TDD::testGPflower(): povodny stav:" << endl;                   // bez operatora OP_LEFT
    g1.toOutFormat();
    g1.flower(&fs, OP_LEFT);
    cout << "TDD::testGPflower(): povodny stav g1 po rozviti:" << endl;
    g1.toOutFormat();

    cout << "--" << endl;

    cout << "TDD::testGPflower(): povodny stav:" << endl;                   // bez operatora OP_RIGHT
    g1.toOutFormat();
    g1.flower(&fs, OP_RIGHT);
    cout << "TDD::testGPflower(): povodny stav g1 po rozviti:" << endl;
    g1.toOutFormat();

    cout << "--" << endl;
    cout << "Testovanie rozvijania novych stavov" << endl;
    cout << "-----------------------------------" << endl;

    // podstata testov - kazdy operator sa rozvije s kazdym inym operatorom v jednom volani metody flower()
    // ale kazdy s kazdym bude rozvity az po volani troch volani flower()
    // filozofie je otestovat dvojice kombinacii, nie vsetky mozne kombinacie pocitajuc a pocet rozvijanych prvkov sucasne

    cout << "--" << endl;
    cout << "OP_LEFT SINGLE" << endl;

    GamePlay g2(1, 2);
    g2.toOutFormat();
    g2.flower(&fs, OP_UP);
    TDD::test_prtFSS(1, 2, &fs);

    cout << "--" << endl;
    cout << "OP_RIGHT SINGLE" << endl;

    g2.appOpr(OP_LEFT);
    g2.toOutFormat();
    g2.flower(&fs, OP_UP);
    TDD::test_prtFSS(1, 2, &fs);

    cout << "--" << endl;
    cout << "OP_UP SINGLE" << endl;

    GamePlay g3(2, 1);
    g3.toOutFormat();
    g3.flower(&fs, OP_LEFT);
    TDD::test_prtFSS(2, 1, &fs);

    cout << "--" << endl;
    cout << "OP_DOWN SINGLE" << endl;

    g3.appOpr(OP_UP);
    g3.toOutFormat();
    g3.flower(&fs, OP_LEFT);
    TDD::test_prtFSS(2, 1, &fs);

    cout << "--" << endl;
    cout << "tri operatory, 1) OP_UP, OP_LEFT, OP_RIGHT" << endl;

    GamePlay g4(3, 3);
    g4.test_setState(9, "1", "2", "3", "4", "x", "5", "6", "7", "8");
    g4.toOutFormat();
    g4.flower(&fs, OP_DOWN);
    TDD::test_prtFSS(3, 3, &fs);

    cout << "--" << endl;
    cout << "tri operatory, 2) OP_DOWN, OP_LEFT, OP_RIGHT" << endl;

    g4.toOutFormat();
    g4.flower(&fs, OP_UP);
    TDD::test_prtFSS(3, 3, &fs);

    cout << "--" << endl;
    cout << "tri operatory, 3) OP_DOWN, OP_UP, OP_RIGHT (false)" << endl;

    g4.appOpr(OP_RIGHT);
    g4.toOutFormat();
    g4.flower(&fs, OP_LEFT);
    TDD::test_prtFSS(3, 3, &fs);
}


// =======================================================================================
// void TDD::testGPgetSize()
// =======================================================================================

void TDD::testGPgetSize()
{
    TDD::prtHeader("void TDD::testGPgetSize() - test spravnosti vypoctu pribliznej velkosti obsadenej pamete jednym stavom");

    cout << "sizeof:: string = " << sizeof(string) << " B" << endl;
    cout << "sizeof:: vector<string> = " << sizeof(vector<string>) << " B" << endl;

    cout << "--" << endl;
    GamePlay g1(1, 2);
    cout << "sizeof:: 1x2, size = " << g1.getStateMemSize() << endl;

    cout << "--" << endl;
    GamePlay g11(2, 1);
    cout << "sizeof:: 2x1, size = " << g11.getStateMemSize() << endl;

    cout << "--" << endl;
    GamePlay g2(2, 2);
    cout << "sizeof:: 2x2, size = " << g2.getStateMemSize() << endl;

    cout << "--" << endl;
    GamePlay g3(3, 3);
    cout << "sizeof:: 3x3, size = " << g3.getStateMemSize() << endl;

    cout << "--" << endl;
    GamePlay g4(51, 5);
    cout << "sizeof:: 51x5, size = " << g4.getStateMemSize() << endl;
}


// =======================================================================================
// void TDD::testGPop<<s()
// =======================================================================================

void TDD::testGPops()
{
    TDD::prtHeader("void TDD::testGPoprs() - test operatorov");

    bool x = false;

    // -------------------------------------------------------------------------
    // OP_UP :: 3x3, pravy stlpec
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): Operator: >>>>>>>>>>>> OP_UP <<<<<<<<<<<<" << endl << "--" << endl;
    cout << "testGPops(): OP_UP 3x3, pravy stlpec" << endl;
    GamePlay g(3, 3);
    g.toOutFormat();

    x = g.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g.toOutFormat();

    x = g.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g.toOutFormat();

    x = g.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g.toOutFormat();

    x = g.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_UP :: 3x3, lavy stlpec
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_UP 3x3, lavy stlpec" << endl;
    GamePlay g1(3, 3);
    g1.test_setState(9, "1", "2", "3", "4", "5", "6", "x", "7", "8");
    g1.toOutFormat();

    x = g1.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g1.toOutFormat();

    x = g1.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g1.toOutFormat();

    x = g1.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g1.toOutFormat();

    x = g1.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g1.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_UP :: 2x1
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_UP 2x1" << endl;
    GamePlay g2(2, 1);
    g2.toOutFormat();

    x = g2.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g2.toOutFormat();

    x = g2.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g2.toOutFormat();

    x = g2.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g2.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_UP, 1x2
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_UP 1x2, pravy stlpec" << endl;
    GamePlay g3(1, 2);
    g3.toOutFormat();

    x = g3.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g3.toOutFormat();

    x = g3.appOpr(OP_UP);
    cout << "testGPops(): OP_UP: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g3.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_DOWN :: 3x3, pravy stlpec
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): Operator: >>>>>>>>>>>> OP_DOWN <<<<<<<<<<<<" << endl << "--" << endl;
    cout << "testGPops(): OP_DOWN 3x3, pravy stlpec" << endl;
    GamePlay g4(3, 3);
    g4.test_setState(9, "1", "2", "x", "4", "5", "6", "7", "8");
    g4.toOutFormat();

    x = g4.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g4.toOutFormat();

    x = g4.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g4.toOutFormat();

    x = g4.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g4.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_DOWN :: 3x3, lavy stlpec
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_DOWN 3x3, lavy stlpec" << endl;
    GamePlay g5(3, 3);
    g5.test_setState(9, "x", "1", "2", "3", "4", "5", "6", "7", "8");
    g5.toOutFormat();

    x = g5.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g5.toOutFormat();

    x = g5.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g5.toOutFormat();

    x = g5.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g5.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_DOWN :: 2x1
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_DOWN 2x1" << endl;
    GamePlay g6(2, 1);
    g6.test_setState(2, "x", "1");
    g6.toOutFormat();

    x = g6.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g6.toOutFormat();

    x = g6.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g6.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_DOWN :: 1x2
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_DOWN 1x2" << endl;
    GamePlay g7(1, 2);
    g7.toOutFormat();

    x = g7.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g7.toOutFormat();

    x = g7.appOpr(OP_DOWN);
    cout << "testGPops(): OP_DOWN: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g7.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_RIGHT :: 1x3
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): Operator: >>>>>>>>>>>> OP_RIGHT <<<<<<<<<<<<" << endl << "--" << endl;
    cout << "testGPops(): OP_RIGHT 1x3" << endl;
    GamePlay g8(1, 3);
    g8.test_setState(3, "x", "1", "2");
    g8.toOutFormat();

    x = g8.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g8.toOutFormat();

    x = g8.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g8.toOutFormat();

    x = g8.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g8.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_RIGHT :: 3x1
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_RIGHT 3x1" << endl;
    GamePlay g9(3, 1);
    g9.test_setState(3, "x", "1", "2");
    g9.toOutFormat();

    x = g9.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g9.toOutFormat();

    g9.appOpr(OP_DOWN);
    x = g9.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g9.toOutFormat();

    g9.appOpr(OP_DOWN);
    x = g9.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g9.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_RIGHT :: 3x3 stredny riadok
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_RIGHT 3x3, stredny riadok" << endl;
    GamePlay g10(3, 3);
    g10.test_setState(9, "1", "2", "3", "4", "x", "5", "6", "7", "8");
    g10.toOutFormat();

    x = g10.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g10.toOutFormat();

    x = g10.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g10.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_RIGHT :: 3x3 posledny riadok
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_RIGHT 3x3, posledny riadok" << endl;
    GamePlay g11(3, 3);
    g11.test_setState(9, "1", "2", "3", "4", "5", "6", "x", "8", "9");
    g11.toOutFormat();

    x = g11.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g11.toOutFormat();

    x = g11.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g11.toOutFormat();

    x = g11.appOpr(OP_RIGHT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g11.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_LEFT :: 1x2
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): Operator: >>>>>>>>>>>> OP_LEFT <<<<<<<<<<<<" << endl << "--" << endl;
    cout << "testGPops(): OP_LEFT 1x2" << endl;
    GamePlay g12(1, 2);
    g12.toOutFormat();

    x = g12.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g12.toOutFormat();

    x = g12.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g12.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_LEFT :: 1x3
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_LEFT 1x3" << endl;
    GamePlay g13(1, 3);
    g13.toOutFormat();

    x = g13.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g13.toOutFormat();

    x = g13.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g13.toOutFormat();

    x = g13.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g13.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_LEFT :: 3x1
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_LEFT 3x1" << endl;
    GamePlay g14(3, 1);
    g14.test_setState(3, "x", "1", "2");
    g14.toOutFormat();

    x = g14.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g14.toOutFormat();

    g14.appOpr(OP_DOWN);
    x = g14.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g14.toOutFormat();

    g14.appOpr(OP_DOWN);
    x = g14.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g14.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_LEFT :: 3x3 stredny riadok
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_LEFT 3x3 stredny riadok" << endl;
    GamePlay g15(3, 3);
    g15.test_setState(9, "1", "2", "3", "4", "x", "5", "6", "7", "8");
    g15.toOutFormat();

    x = g15.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g15.toOutFormat();

    x = g15.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g15.toOutFormat();

    // -------------------------------------------------------------------------
    // OP_LEFT :: 3x3 spodny riadok
    // -------------------------------------------------------------------------

    cout << "--" << endl;
    cout << "testGPops(): OP_LEFT 3x3 spodny riadok" << endl;
    GamePlay g16(3, 3);
    g16.toOutFormat();

    x = g16.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g16.toOutFormat();

    x = g16.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g16.toOutFormat();

    x = g16.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g16.toOutFormat();

    x = g16.appOpr(OP_LEFT);
    cout << "testGPops(): OP_RIGHT: "; (x == false) ? cout << "FALSE" : cout << "TRUE"; cout << ", result: " << endl;
    g16.toOutFormat();


    /// \todo hranicne hodnoty
}


// =======================================================================================
// void TDD::testGPsetStav()
// =======================================================================================

void TDD::testGPsetStav()
{
    TDD::prtHeader("void TDD::testGPsetStav()");

    cout << "--" << endl;
    cout << "Povodny stav: " << endl;
    GamePlay g1(2, 3);
    g1.toOutFormat();

    State s;
    s.resize(6, "a");
    s.at(0) = emptyBox;
    for (uint8_t i = 5; i >= 1; i--)
        s.at(i) = Utils::digit2string(i);

    cout << "Novy stav: " << endl;
    g1.setState(&s);
    g1.toOutFormat();

    State s1;
    s1.resize(5);
    State s2;
    s2.resize(7);

    // PASS
    // g1.setStav(&s1);

    // PASS
    //g1.setStav(&s2);
}


void TDD::testGPswap()
{
    TDD::prtHeader("void TDD::testGPswap()");

    /*
    GamePlay g(3, 3);

    cout << "TDD::testGPswap(): create GamePlay(3, 3)" << endl;
    cout << "TDD::testGPswap(): state: " << endl;
    g.toOutFormat();

    g.swapStateBox(0, 8);
    cout << "TDD::testGPswap(): swap(0, 8): " << endl;
    g.toOutFormat();

    g.swapStateBox(8, 0);
    cout << "TDD::testGPswap(): swap(8, 0): " << endl;
    g.toOutFormat();

    */
    /// \todo hranicne hodnoty
}


// =======================================================================================
//                                     PRIVATE
// =======================================================================================

// =======================================================================================
// void TDD:pritHeader(string header)
// =======================================================================================

void TDD::prtHeader(string head)
{
    cout << "==============================================================================" << endl;
    cout << "            " << head << endl;
    cout << "==============================================================================" << endl;
}


// =======================================================================================
// void TDD::test_prtFSS(int M, int N, FlowerStates const * const f)
// =======================================================================================

/// Pomocna funkcia pre TDD - vypis stavov z FlowerStates

/// \warning
/// - Experimental
/// - Don't tested
/// - Mimo rozumny navrh

void TDD::test_prtFSS(int M, int N, const FlowerStates * const f)
{
    FlowerStates::const_iterator it;

    if (f->empty() == true) {
        cout << "TDD::test_prtFSS(): EMPTY - ziaden NOVY STAV" << endl;
        return;
    }

    for (it = f->begin(); it != f->end(); it++) {
        cout << "-->" << GamePlay::oprToStr((*it).op) << "<--" << endl;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                cout << (*it).s.at(i*N+j) << " ";
            }
          cout << endl;
        }
    }
}
