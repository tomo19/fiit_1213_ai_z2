#include "GamePlay.hh"
#include "GamePlay_common.hh"
#include "FS.hh"
#include "Utils.hh"
#include "State.hh"
#include <cassert>
#include <cstdarg>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

// =======================================================================================
//                                        PUBLIC
// =======================================================================================

// =======================================================================================
// GamePlay::GamePlay(uint8_t M, uint8_t N)
// =======================================================================================

/// Konstruktor

///
/// \param M - pocet riadkov
/// \param N - pocet stlpcov
///
/// Vytvori novy model hry.
/// - pocet policiek: MxN
/// - implicitny stav:
///  - cisla usporiadane zlava hore <1, 2, ..., MxN-1)
///  - posledne policko prazdne: x
///
/// \pre
/// - MxN <= 255
/// - M != N != 1
/// - M != 0
/// - N != 0
///
/// \warning rozhodena zavislost, na format \ref State

GamePlay::GamePlay(uint8_t M, uint8_t N)
{
    // osetrenie vstupu: ziadna nula (0x0), (0x1), (1x0)
    assert(!((M == 0) || (N == 0)));

    // osetrenie vstupu: 1x1 nieje povoleny
    assert(!((M == 1) && (N == 1)));

    // osetrenie vstupu: viac ako 255 policok spolu
    assert(M*N <= 255);      /// \todo KONSTANTA: 255, MAX(uint8_t)

    this->M = M;
    this->N = N;
    this->MN = M*N;

    // vypocitaj pribliznu velkost pamete alokovanej pre 1 stav
    this->stateMemSize = this->computeStateMemSize();

    // vymaz vsetky prvky vektora
    this->as.clear();

    // nastav velkost vektora exaktne na pocet policok hracej plochy
    this->as.resize(this->MN);

    // nastav implicitny stav
    for (uint8_t i = 0; i < (MN-1); i++) {
        this->as.at(i) = Utils::digit2string(i+1);
    }

    // nastav posledne policko prazdne
    this->as.at(MN-1) = emptyBox;
}


// =======================================================================================
// bool GamePlay::appOpr(Operator op)
// =======================================================================================

/// Aplikuj operator

///
/// - aplikuje operator na aktualny stav this->as
/// - aktualny stav sa po aplikovani operatora zmeni na novy sucasny stav, ak bolo operator mozne aplikovat
///
/// - zoznam operatorov: \ref #Operator
///
/// \param op operator, \ref #Operator
/// \retval false operator sa neda aplikovat na sucasny stav
/// \retval true operator bol uspesne aplikovany, vysledok v s
///
/// \warning rozhodena zavislost, na format \ref State

bool GamePlay::appOpr(Operator op)
{
    uint8_t epos = 0;                    // pozicia prazdneho policka

    epos = this->findEmptyBox();


    // OP_UP: operator presun prazdneho policka nahor

    if (op == OP_UP) {
        // ak sme na prvom riadku
        if (epos <= ((this->N)-1)) {

#ifdef DEBUG_GPAPPOR
            cerr << "GamePlay::appOpr(): OP_UP, epos=" << (unsigned int) epos << ", FALSE" << endl;
#endif

            return false;
        }

#ifdef DEBUG_GPAPPOR
        cerr << "GamePlay::appOpr(): OP_UP, epos=" << (unsigned int) epos << endl;
#endif

        // vymen prvky (posun prazdne policko o riadok vyssie :: sucasne prazdne policko - pocet stlpcov
        this->swapBox(epos, epos - this->N);

        return true;
    }

    // OP_DOWN: operator presun prazdneho policka nadol

    if (op == OP_DOWN) {
        // ak sme na poslednom riadku
        if (epos >= ((this->MN)-this->N)) {

#ifdef DEBUG_GPAPPOR
            cerr << "GamePlay::appOpr(): OP_DOWN, epos=" << (unsigned int) epos << ", FALSE" << endl;
#endif

            return false;
        }

#ifdef DEBUG_GPAPPOR
        cerr << "GamePlay::appOpr(): OP_DOWN, epos=" << (unsigned int) epos << endl;
#endif

        // vymen prvky (posun prazdne policko o riadok nizsie :: sucasne prazdne policko + pocet stlpcov
        this->swapBox(epos, epos + this->N);

        return true;
    }

    // OP_RIGHT: operator presun prazdneho policka nadol

    if (op == OP_RIGHT) {
        // ak sme na absolutnej poslednej pozicii(dolny pravy roh) alebo na poslednejj pozicii riadku
        if ((epos == (this->MN-1)) || ((epos+1)%this->N==0)) {

#ifdef DEBUG_GPAPPOR
            cerr << "GamePlay::appOpr(): OP_RIGHT, epos=" << (unsigned int) epos << ", FALSE" << endl;
#endif

            return false;
        }

#ifdef DEBUG_GPAPPOR
        cerr << "GamePlay::appOpr(): OP_RIGHT, epos=" << (unsigned int) epos << endl;
#endif

        this->swapBox(epos, epos+1);

        return true;
    }

    // OP_LEFT: operator presun prazdneho policka nadol

    if (op == OP_LEFT) {
        if ((epos%this->N) == 0) {

#ifdef DEBUG_GPAPPOR
            cerr << "GamePlay::appOpr(): OP_LEFT, epos=" << (unsigned int) epos << ", FALSE" << endl;
#endif

            return false;
        }

#ifdef DEBUG_GPAPPOR
        cerr << "GamePlay::appOpr(): OP_LEFT, epos=" << (unsigned int) epos << endl;
#endif

        this->swapBox(epos, epos-1);

        return true;
    }

    // nedefinovany operator
#ifdef DEBUG_GPAPPOR
    cerr << "GamePlay::appOpr(): Undefined operator" << endl;
#endif

    assert(0);

    return false;
}


// =======================================================================================
// void GamePlay::flower(FlowerStates *const f, Operator notOp)
// =======================================================================================

/// Rozvije aktualny stav

///
/// - meni aktualny stav, po skonceni metody ostane aktualny stav zachovany
///
/// Algoritmus:
/// - postupne aplikuje vsetky operatory na aktualny stav
/// - po uspesnom aplikovani operatora obnovy povodny stav pomocou this.setState(cas);
/// - aktualny stav sa obnovuje iba v pripade uspesneho aplikovania operatora
///
/// \param f adresa premennej, na ktoru sa ulozi zoznam novo-rovitych uzlov
/// \param notOp jeden operator, ktory sa nema aplikovat (vhodne pre algoritmy hladanie, ked sa nema aplikovat inverzny operator voci operatoru, pomocou ktoreho vznikol uzol, ktory ideme rozvijat)
/// \pre f != NULL
/// \pre op je platny operator
/// \post f je najskor zmazany a potom naplneny novymi stavmi
/// \test notOp = Operator::OP_ROOT

void GamePlay::flower(FlowerStates *const f, Operator notOp)
{
    assert(f != NULL);
    assert(notOp != OP_NOOP);

    FlowerState fs;                                         // novy rozvity stav (polozka zoznamu FlowerStates, f parameter

    State cas;                                              // cas - kopia aktualneho stavu
    this->copyState(&(this->as), &cas);

    bool res = false;

    // vycisti obsah f
    f->clear();

#ifdef DEBUG_GPFLOWER
    cerr << "GamePlay::flower(): -- start  --,  stav this.as=" << this->toStr() << endl;
#endif

    // OP_UP

    if(notOp != OP_UP) {
        res = this->appOpr(OP_UP);
        if (res == true) {
            fs.op = OP_UP;
            this->copyState(&(this->as), &fs.s);
            f->push_back(fs);
            this->setState(&cas);                               // obnov povodny stav z cas
        }

#ifdef DEBUG_GPFLOWER
        cerr << "GamePlay::flower(): OP_UP    DONE: stav this.as=" << this->toStr() << endl;
#endif

    }

    // OP_DOWN

    if(notOp != OP_DOWN) {
        res = this->appOpr(OP_DOWN);
        if (res == true) {
            fs.op = OP_DOWN;
            this->copyState(&(this->as), &fs.s);
            f->push_back(fs);
            this->setState(&cas);                               // obnov povodny stav z cas
        }

#ifdef DEBUG_GPFLOWER
        cerr << "GamePlay::flower(): OP_DOWN  DONE: stav this.as=" << this->toStr() << endl;
#endif

    }

    // OP_LEFT

    if(notOp != OP_LEFT) {
        res = this->appOpr(OP_LEFT);
        if (res == true) {
            fs.op = OP_LEFT;
            this->copyState(&(this->as), &fs.s);
            f->push_back(fs);
            this->setState(&cas);                               // obnov povodny stav z cas
        }

#ifdef DEBUG_GPFLOWER
        cerr << "GamePlay::flower(): OP_LEFT  DONE: stav this.as=" << this->toStr() << endl;
#endif

    }

    // OP_RIGHT

    if(notOp != OP_RIGHT) {
        res = this->appOpr(OP_RIGHT);
        if (res == true) {
            fs.op = OP_RIGHT;
            this->copyState(&(this->as), &fs.s);
            f->push_back(fs);
            this->setState(&cas);                               // obnov povodny stav z cas
        }

#ifdef DEBUG_GPFLOWER
        cerr << "GamePlay::flower(): OP_RIGHT DONE: stav this.as=" << this->toStr() << endl;
#endif

    }

#ifdef DEBUG_GPFLOWER
    cerr << "GamePlay::flower(): -- end --,     stav this.as=" << this->toStr() << endl;
#endif
}


// =======================================================================================
// enum Operator GamePlay::getInvOpr(enum Operator op);
// =======================================================================================

/// Inverzny operator

/// \param op operator, pre ktory chceme najst inverzny operator
/// \retval enum Operator, \ref #Operator, inverzny operator voci operatoru zadaneho ako parameter op

enum Operator GamePlay::getInvOpr(enum Operator op)
{
    assert(op != OP_NOOP);

    if (op == OP_UP)
        return OP_DOWN;

    if (op == OP_DOWN)
        return OP_UP;

    if (op == OP_RIGHT)
        return OP_LEFT;

    if (op == OP_LEFT)
        return OP_RIGHT;

    if (op == OP_ROOT)
        return OP_ROOT;

    // iba pre compiler warning uspokojenie
    // nikdy nenastane
    // !!! predsa nastalo - dopisanie noveho operatora (OP_ROOT), zabudol sa mu definovat inverzny operator => dostalo sa to sem => dalsie volanie uzol s op = OP_NOOP
    return OP_NOOP;
}


// =======================================================================================
// int GamePlay::getStateMemSize()
// =======================================================================================int GamePlay::getStateMemSize()

/// Pribliznu velkost pouzitej pamete alokovanej pre jeden stav

/// Metodika vypoctu:
/// - size = sizeof(vector<string>) + [(M*N) * sizeof(string)] + pocet jednomiestnych cisel + pocet dvojmiestnych cisel + pocet trojmiestnych cisel
///
/// \warning
/// - hodnota je iba orientacna
/// - string moze mat v skutocnosti alokovanu vacsiu kapacitu ak to nieje explicitne pozadovane v zdrojovom kode (string.capacity())
/// - skutocna kapacita je o nieco vacsia, niesu zaratane vsetky cleny tried std::vector a std::string
///
/// \return orientacna velkost pozadovaj pamete pre ulozenie jedneho stavu [B]

int GamePlay::getStateMemSize()
{
    return this->stateMemSize;
}


// =======================================================================================
// void GamePlay::setState(const State *const s)
// =======================================================================================

/// Nastavy aktualny stav na novy stav

///
/// - obsah s skopiruje do this.as
/// - nekotroluje integritu stavu
/// - funkcia musi byt rychla, pouzivana algoritmamy hladania
///
/// \param s novy stav, nekontroluje sa jeho integrita
/// \pre
/// - spravna integrita stavu (cislice v poriadku, neopakujuce sa cislice a pod.), integrita sa nekontroluje!
///
/// \warning rozhodena zavislost, na format \ref State

void GamePlay::setState(const State *const s)
{
    // s == NULL
    assert(s != NULL);

    // dlzka vektora stavov musi byt rovnaka
    assert(!(s->size() != this->MN));

    // skopiruj
    for (uint8_t i = 0; i < this->MN; i++) {
        this->as.at(i) = s->at(i);
    }
}


// =======================================================================================
// void GamePlay::toOutFormat()
// =======================================================================================

/// Zobrazi reprezentaciu aktualneho stavu \ref as "this.as" na standartny vystup ako foramtovany string retazec - mriezka.

///
/// \pre STL <iostream>, cout object

void GamePlay::toOutFormat()
{
    cout << this->toStrFormat() << endl;
}


// =======================================================================================
// string GamePlay::toStr()
// =======================================================================================

/// Reprezentacia aktualneho stavu \ref as "this.as" ako string retazec (v jednom riadku).

/// \return retazec predstavuju aktualny stav \ref this.as
///

string GamePlay::toStr()
{
    string s;
    s.clear();

    for (uint8_t i = 0; i < this->as.size(); i++) {
        s.append(this->as.at(i));
        s.append(" ");
    }

    return s;
}


// =======================================================================================
// string GamePlay::toStrFormat()
// =======================================================================================

/// Reprezentacia aktualneho stavu \ref as "this.as" ako formatovany string retazec (textova mriezka).

///
/// \return retazec predstavuju aktualny stav \ref this.as
/// \warning rozhodena zavislost, na format \ref State

string GamePlay::toStrFormat()
{
    ostringstream os;
    string s;
    s.clear();

    for (uint8_t i = 0; i < this->M; i++) {
        for (uint8_t j = 0; j < this->N; j++) {
            os << this->as.at((i*this->N)+j) << " "; // pozicia: riadok * pocet stlpcov + index v stlpci
        }
        if (i != (this->M-1))
            os << endl;
    }

    s.append(os.str());

    return s;
}


// =======================================================================================
//                                       PRIVATE
// =======================================================================================

/// Vypocita pribliznu velkost pouzitej pamete alokovanej pre jeden stav

/// Metodika vypoctu:
/// - size = sizeof(vector<string>) + [(M*N) * sizeof(string)] + pocet jednomiestnych cisel + pocet dvojmiestnych cisel + pocet trojmiestnych cisel
///
/// \pre
/// - this.MN - korektna hodnota
/// - metoda by sa mala volat iba z konstruktora
/// \post this.stateMemSize - velkost jedneho Stavu [B]
/// \warning
/// - hodnota je iba orientacna
/// - string moze mat v skutocnosti alokovanu vacsiu kapacitu ak to nieje explicitne pozadovane v zdrojovom kode (string.capacity())
/// - skutocna kapacita je o nieco vacsia, niesu zaratane vsetky cleny tried std::vector a std::string
/// - rozhodena zavislost, na format \ref State
///
/// \return orientacna velkost pozadovaj pamete pre ulozenie jedneho stavu [B]

int GamePlay::computeStateMemSize()
{
    int m1 = 0;                 // 1-miestne cislice
    int m2 = 0;                 // 2-miestne cislice
    int m3 = 0;                 // 3-miestne cislice
    int capacity = 0;

    // pripocitaj velkost triedy vector
    capacity += sizeof(vector<string>);

    // pripocitaj velkost triedy string nasobenej poctom polickom na hracej ploche
    capacity += (sizeof(string) * this->MN);

    // pripocitaj velkost poctu znakov na hracej ploche - obsah jednotlivych retazcov kazdeho z policka

    // prazdne policko
    capacity += emptyBox.size();

    // cislice (1-, 2- a 3-miestne)

    m1 = m2 = m3 = 0;

    for (uint8_t i = 1; i <= (this->MN-1); i++) {
        if (i < 10) {
            m1++;
            continue;
        }
        if (i < 100) {
            m2++;
            continue;
        }
        if (i < 255) {                    /// \todo KONSTANTA: 255, MAX(uint8_t)
            m3++;
            continue;
        }
    }

    capacity += (m1 + m2 + m3);

#ifdef DEBUG_GPCOMPUTSSIZE
    cerr << "GamePlay::computeStateMemSize(): sizeof:: vector<string> = " << sizeof(vector<string>) << endl;
    cerr << "GamePlay::computeStateMemSize(): sizeof:: string = " << sizeof(string) << endl;
    cerr << "GamePlay::computeStateMemSize(): sizeof:: MN*string = " << this->MN * sizeof(string) << endl;
    cerr << "GamePlay::computeStateMemSize(): emptyBox.size() = " << emptyBox.size() << endl;
    cerr << "GamePlay::computeStateMemSize(): m1 = " << m1 << endl;
    cerr << "GamePlay::computeStateMemSize(): m2 = " << m2 << endl;
    cerr << "GamePlay::computeStateMemSize(): m3 = " << m3 << endl;
    cerr << "GamePlay::computeStateMemSize(): m1 + m2 + m3 = " << (m1 + m2 + m3) << endl;
    cerr << "GamePlay::computeStateMemSize(): capacity = " << capacity << endl;
#endif

    return capacity;
}


// =======================================================================================
// void GamePlay::copyState(const State *const src, State *const dst)
// =======================================================================================

/// Kopiruje stav State

/// \param src adresa stavu, ktory sa bude kopirovat do dst
/// \param dst adresa stavu, do ktoreho sa bude kopirovat stav src
/// \pre src a dst nieje ten isty stav (kontroluje sa)
/// \pre src v integritnom stave (NEkontroluje sa)
/// \pre src != dst != NULL (kontroluje sa)
/// \pre src by nemal mat nulovu velkost, algoritmus to ale vie spracovat
/// \pre dst moze byt vektor roznej velkosti
/// \post dst ma rovnaky pocet prvkov ako src
///
/// \note Metoda musi byt rychla, pouziva sa (nepriamo) algoritmamy hladania, vramci procesu rozvitia stavu.
/// \warning rozhodena zavislost, na format \ref State

void GamePlay::copyState(const State *const src, State *const dst)
{
    assert((src != NULL) && (dst != NULL) && (src != dst));

    /// \todo
    /// - DEPLOY: zryychlit - nezistovat velkost 2x ( uint[>8]_t -> cast to uint8_t) \n
    /// - KONSTANTA: 255, MAX(uint8_t)
    assert(src->size() <= 255);

    uint8_t source_size = 0;

    source_size = src->size();

    // zmaz cely obsah destination stavu
    dst->clear();

    // nastav velkost vektora destination stavu na rovnaku velku
    dst->resize(source_size);

    // skopiruj vsetky prvky src do dst
    for (uint8_t i = 0; i < src->size(); i++) {
        dst->at(i) = src->at(i);
    }
}


// =======================================================================================
// uint8_t GamePlay::findEmptyBox(State *const s)
// =======================================================================================

/// Najde poziciu prazdneho policka

/// \pre \ref #emptyBox
/// \return index do pola(vektora) \ref State, kde sa nachadza prazdne policko
///
/// \warning rozhodena zavislost, na format \ref State


uint8_t GamePlay::findEmptyBox()
{
    bool wasFinded = false;
    uint8_t pos = 0;

    // prejdi cely vektor stavu
    for (uint8_t i = 0; i < this->as.size(); i++) {
        // nasli sme prazdne policky na pozicii ...
        if (this->as.at(i) == emptyBox) {
            pos = i;
            wasFinded = true;
        }
    }

    assert(wasFinded != false);

    return pos;
}


// =======================================================================================
// void GamePlay::swapStateBox(uint8_t p1, uint8_t p2);
// =======================================================================================

/// Vymeni dva policka v aktualnom stave hracej plochy \ref as "this.as"

/// \param p1 index do pola(vektora) prveho policka
/// \param p2 index do pola(vektora) druheho policka
///
/// \pre
/// - p1, p2 v rozsah <0, MN-1>
///
/// \warning rozhodena zavislost, na format \ref State

void GamePlay::swapBox(uint8_t p1, uint8_t p2)
{
    assert(!((p1 > (this->MN-1)) || (p2 > (this->MN-1))));

    string stmp = this->as.at(p1);

    this->as.at(p1) = this->as.at(p2);

    this->as.at(p2) = stmp;
}


// =======================================================================================
//                                      TDD
// =======================================================================================

// =======================================================================================
// void GamePlay::test_findEmptyBoxT(State *const s)
// =======================================================================================

/// Testovanie \ref findEmptyBox()

/// Metoda je volana z \ref TDD::testGPemptyBox()
/// \warning
/// - EXPERIMENTAL
/// - used only by TDD
/// - moze sa odstranit spolu TDD testami

void GamePlay::test_findEmptyBox()
{
    cout << "GamePlay::TDDfindEmptyBox(:)" << endl;
    this->toOutFormat();
    cout << (unsigned int) this->findEmptyBox() << endl;
    this->swapBox(0, 1);
    this->toOutFormat();
    cout << (unsigned int) this->findEmptyBox() << endl;
}


// =======================================================================================
// void GamePlay::test_setState(const string s)
// =======================================================================================

/// Nastavy aktualny stav na novy stav

///
/// \param n pocet policok v MxN hlavolane
/// \param ... n-krat string arg, kazdy retazec predstavuje hodnotu jedneho policka, s indexom od 0 po n
///
/// - priklad: test_setState(4, "1", "2", "3", "x");
/// - pouzita iba v testoch pre ulahcenie nastavovania noveho stavu
///
/// \note - rozhodena zavislost, na format \ref State
/// \warning
/// - EXPERIMENTAL
/// - used only by TDD
/// - moze sa odstranit spolu TDD testami

void GamePlay::test_setState(int n, ...)
{
    va_list args;
    va_start(args, n);

    const char *a = NULL;
    string s;

    for (int i = 0; i < n; i++) {
        a = va_arg(args, const char *);
        s.clear();
        s.append(a);
        this->as.at(i) = s;
    }

    va_end(args);
}


// =======================================================================================
//                                    PUBLIC STATIC
// =======================================================================================

// =======================================================================================
// void GamePlay::oprToStr(Operator op)
// =======================================================================================

/// Konverzia enum hodnoty operatora na retazec

string GamePlay::oprToStr(Operator op)
{
    string s;
    s.clear();

    if (op == OP_UP)
        s.append("OP_UP");
    if (op == OP_DOWN)
        s.append("OP_DOWN");
    if (op == OP_RIGHT)
        s.append("OP_RIGHT");
    if (op == OP_LEFT)
        s.append("OP_LEFT");
    if (op == OP_NOOP)
        s.append("OP_NOOP");
    if (op == OP_ROOT)
        s.append("OP_ROOT");

    return s;
}
