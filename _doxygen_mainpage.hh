/// \mainpage Artifical Intelligence - Problems solving, searching in state space \n
/// \image html agent.png "Search agent"
///
/// <b>Project no.: 2</b> \n
/// \version 0.1
/// \author Tomáš Kundis
///
/// <b>Implementation:</b>
/// <ul>
/// <li>C++, v. C++11 (formerly known as C++0x)</li>
/// <li>STL library TR1 extension</li>
/// </ul>
///
/// <table><tr><td>
/// Umelá inteligencia \n
/// FIIT, STU v Bratislave \n
/// letný semester \n
/// 3. ročník \n
/// ak.r.: 2012/2013 \n
/// </table></td></tr>
