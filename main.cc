#include "AL.hh"
#include "ABSearch.hh"
#include "AGSearching.hh"
#include "TDD.hh"
#include "Utils.hh"
#include "FIFO.hh"
#include "LIFO.hh"
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main(int argc, char *argv[])
{
    //TDD::testAll();
    //TDD::testFIFO();
    //TDD::testLIFO();
    //TDD::testGPConstructorAndOutputs();
    //TDD::testGPsetStav();
    //TDD::testGPswap();
    //TDD::testGPemptyBox();
    //TDD::testGPops();
    //TDD::testGPflower();
    //TDD::testGPgetSize();

    if (Utils::processArguments(argc, argv, &::margs) == false) {
        cerr << "ERROR: main():    bad arguments!" << endl;
        return EXIT_FAILURE;
    }

    if (Utils::readInputFile(margs.fileIn, &margs.sstate, &margs.fstate, &margs.M, &margs.N) == false) {
        cerr << "ERROR: main(): readInputFile(): FALSE" << endl;
        return EXIT_FAILURE;
    }

    cerr << "main(): M: " << (unsigned int) margs.M << "; N: " << (unsigned int) margs.N << endl;
    cerr << "main(): sstate: " << Utils::state2str(&margs.sstate) << endl;
    cerr << "main(): fstate: " << Utils::state2str(&margs.fstate) << endl;

    /*
    FIFO *f1 = new FIFO;
    LIFO *f2 = new LIFO;

    AGSearch *a1 = new AGSearch(11, f1, margs.M, margs.N, margs.sstate, margs.fstate, 20, 40000000);
    AGSearch *a2 = new AGSearch(22, f2, margs.M, margs.N, margs.sstate, margs.fstate, 20, 40000000);

    a1->runAll();
    //a2->run(3);

    delete a1;
    delete a2;

    delete f1;
    delete f2;
    */

    //ABSearch b(margs.M, margs.N, margs.sstate, margs.fstate, AL::A_BREADTH, 10, 200000000, 1, AL::A_DEPTH, 10, 200000000, 2);
    //ABSearch b(margs.M, margs.N, margs.sstate, margs.fstate, AL::A_BREADTH, 10, 200000000, 1, AL::A_DEPTH, 10, 200000000, 2);
    ABSearch b(margs.M, margs.N, margs.sstate, margs.fstate, margs.a1type, margs.a1maxDepth, margs.a2maxMem, margs.a1runs, margs.a2type, margs.a2maxDepth, margs.a2maxMem, margs.a2runs);

    for (unsigned int i = 0; i < margs.runs; i++) {
        if (b.step() == AL::RET_SUCCESS)
            break;
        if (b.step() == AL::RET_FAILURE)
            break;
    }

    return EXIT_SUCCESS;
}

