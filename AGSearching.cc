#include "AGSearching.hh"
#include "AUtils.hh"
#include "FS.hh"
#include "GamePlay.hh"
#include "GamePlay_common.hh"
#include "IDataStore.hh"
#include "Node.hh"
#include "State.hh"
#include "Utils.hh"
#include <cassert>
#include <cstdint>
#include <iostream>
#include <exception>

using namespace std;

// =======================================================================================
// AGSearch::AGSearch(IDataStore *const front,  uint8_t M, uint8_t N, State sstate, State fstate, uint16_t maxDepth, int maxMem)
// =======================================================================================

/// Konstruktor

///
/// \param front adresa frontu, vytvara sa mimo triedu, urcuje strategiu hladania, uvolnuje sa v destruktore tejto triedy!

AGSearch::AGSearch(unsigned int id, IDataStore *front,  uint8_t M, uint8_t N, State sstate, State fstate, uint16_t maxDepth, int maxMem)
{
    assert(front != NULL);

    // inicializacia clenov
    this->id = 0;
    this->sstate.clear();
    this->fstate.clear();
    this->front = NULL;
    this->maxDepth = 0;
    this->maxMem = 0;
    this->mem = 0;
    this->gm = NULL;
    this->nextId = 0;
    this->countf = 0;
    this->countp = 0;
    this->fnode = NULL;

    this->id = id;
    this->front = front;
    this->sstate = sstate;
    this->fstate = fstate;
    this->maxDepth = maxDepth;
    this->maxMem = maxMem;

    cerr << "AGSearch[" << this->id << "]::AGSearch(): start state: " << Utils::state2str(&this->sstate) << endl;
    cerr << "AGSearch[" << this->id << "]::AGSearch(): finite state: " << Utils::state2str(&this->fstate) << endl;
    cerr << "AGSearch[" << this->id << "]::AGSearch(): maxDepth: " << this->maxDepth << endl;
    cerr << "AGSearch[" << this->id << "]::AGSearch(): maxMem: " << this->maxMem << " B" << endl;

    // vytvor GamePlay pridruzeny instancii triedy
    try {
        this->gm = new GamePlay(M, N);
    }
    catch (bad_alloc &e) {
        cerr << "ERROR: ASearch::ASearch(): new GamePlay: " << e.what() << endl;
    }

    cerr << "AGSearch[" << this->id << "]::AGSearch(): sizeof: Node * (pointer): " << sizeof(Node *) << " B" << endl;
    cerr << "AGSearch[" << this->id << "]::AGSearch(): sizeof: Node: " << sizeof(Node) << " B" << endl;
    cerr << "AGSearch[" << this->id << "]::AGSearch(): sizeof: State: " << this->gm->getStateMemSize() << " B" << endl;

    // KOREN STROMU - zaciatocny stav - vytvor uzol
    Node *tmp = NULL;

    /// \todo THROW: bad_alloc, zachytit vo volajucej procedura, toto je cele zle, z konstruktora inak neosetrie fail pri alokovani pamete
    tmp = AUtils::createNode(this->nextId++, 0, 0, NULL, &this->sstate, OP_ROOT);
    if (tmp == NULL) {
        cerr << "ERROR: AGSearch::AGSearch(): creating root node FAILED (allocation problem)" << endl;
        delete this->gm;
        return;
    }

    this->countf++;
    this->mem += sizeof(Node);
    this->mem += this->gm->getStateMemSize();

    cerr << "AGSearch[" << this->id << "]::AGSearch(): creating root node: " << AUtils::node2str(tmp) << endl;

    // KOREN STROMU - uloz na front
    this->front->push(tmp);
    this->mem += sizeof(Node *);

    cerr << "AGSearch[" << this->id << "]::AGSearch(): rozvite: " << this->countf << "; spracovane: " << this->countp << "; used memory: " << this->mem << " B" << endl;
}


// =======================================================================================
// AGSearch::~AGSearch()
// =======================================================================================

/// Destruktor

///
/// - zrusi objekt this->front vytvoreny mimo triedy, uz potom neuvolnovat!
///   - po navrate desktrukura bude referencia na front ukazovat na NULL
/// - zrusi objekt this->gm (\ref GamePlay)

AGSearch::~AGSearch()
{
    if (this->gm != NULL)
        delete this->gm;

    /// \todo FREE: uvolnit z pamete strom hladania

    // docasne riesenie
    while (this->front->isEmpty() == false)
        delete this->front->pop();

    // uvolni front
    /// \todo ISSUE: zatial neni riesenie (problem: destkruktor konretneho objektu neni definovany ako rozhranie IDataStore, konretne: ~FIFO(), ~LIFO())
}


// =======================================================================================
// bool AGSearch::goalTest(State *const s)
// =======================================================================================

/// Cielovy test

bool AGSearch::goalTest(State *const s)
{
    return Utils::compareState(s, &this->fstate);
}


// =======================================================================================
// void AGSearch::makeInverseSolution(Solution *const solution)
// =======================================================================================

void AGSearch::makeInverseSolution(Solution *const solution)
{
    Node *n = this->fnode;

    assert(solution != NULL);
    assert(n != NULL);

    const Node *tmp = NULL;

    // uloz zaciatocny stav do Solution: tu zaciatocny stav == n
    solution->sstate.resize(n->state.size());

    for (uint8_t i = 0; i < n->state.size(); i++) {
        solution->sstate.at(i) = n->state.at(i);
    }

    // vymaz cely list (mal by byt uz prazdny)
    solution->ops.clear();

    // kopiruj operatory z daneho stavu n do korena, operator na zaciatok zoznamu
    tmp = n;
    while (tmp != NULL) {
        solution->ops.push_back(tmp->op);
        tmp = tmp->parent;
    }

    // odstran OP_ROOT - musi byt posledny operator
    assert(solution->ops.back() == OP_ROOT);

    solution->ops.pop_back();

    // invertuj vsetky stavy
    Operator iopr;

    list<Operator>::iterator it;

    for (it = solution->ops.begin(); it != solution->ops.end(); it++) {
        iopr = this->gm->getInvOpr(*it);
        *it = iopr;
    }
}


// =======================================================================================
// void AGSearch::makeSolution(Solution *const solution)
// =======================================================================================

void AGSearch::makeSolution(Solution *const solution)
{
    Node *n = this->fnode;

    assert(solution != NULL);
    assert(n != NULL);              // na zaciatku nesmie byt dany n = NULL!

    const Node *tmp = NULL;

    // uloz zaciatocny stav do Solution
    solution->sstate.resize(this->sstate.size());                   /// \todo KONSTANTA: drzat sa konstant v takomto pripade, M*N

    for (uint8_t i = 0; i < this->sstate.size(); i++) {
        solution->sstate.at(i) = this->sstate.at(i);
    }

    // vymaz cely list (mal by byt uz prazdny)
    solution->ops.clear();

    // kopiruj operatory z daneho stavu n do korena, operator na zaciatok zoznamu
    tmp = n;
    while (tmp != NULL) {
        solution->ops.push_front(tmp->op);
        tmp = tmp->parent;
    }

    // odstran OP_ROOT - musi byt posledny operator
    assert(solution->ops.front() == OP_ROOT);

    solution->ops.pop_front();
}


// =======================================================================================
// enum AL::ARetStatus run(int interations, enum AL::ARetFlag *const rflag = NULL)
// =======================================================================================

/// Vykona zadany pocet iteracii algoritmu

/// \param *rflag - priznak navratovej hodnoty, moze byt NULL, implicitne NULL
/// \warning neotestovane

enum AL::ARetStatus AGSearch::run(unsigned int iterations, enum AL::ARetFlag *const rflag)
{
    enum AL::ARetStatus ret;

    cerr << "AGSearch[" << this->id << "]::run(): number of iterations: " << iterations << endl;

    for (unsigned int i = 0; i < iterations; i++) {
        ret = this->step(rflag);
        if (ret != AL::RET_CONTINUE)
            return ret;
    }

    if (rflag != NULL)
        *rflag = AL::F_NONE;
    return AL::RET_CONTINUE;
}


// =======================================================================================
// enum AL::ARetStatus runAll(enum AL::ARetFlag *const rflag = NULL)
// =======================================================================================

/// Vykona zadany pocet iteracii algoritmu

/// \param *rflag - priznak navratovej hodnoty, moze byt NULL, implicitne NULL
/// \warning neotestovane

enum AL::ARetStatus AGSearch::runAll(enum AL::ARetFlag *const rflag)
{
    enum AL::ARetStatus ret;

    do {
        ret = this->step(rflag);
    } while(ret == AL::RET_CONTINUE);

    return AL::RET_CONTINUE;
}


// =======================================================================================
// void AGSearch::step()
// =======================================================================================

/// Jedna iteracia algoritmu

/// - vyber uzol 'u' z frontu \ref IDataStore
/// - cielovy test na 'u'
/// - rozvi 'u' (pomocou GamePlay.flower())
/// - nove uzly z predosleho kroku zarad do frontu
///
/// \param *rflag - priznak navratovej hodnoty, moze byt NULL, implicitne NULL

enum AL::ARetStatus AGSearch::step(enum AL::ARetFlag *const rflag)
{
    cerr << "AGSearch[" << this->id << "]::step(): -- " << endl;

    Node *u = NULL;

    // PRAZDNY FRONT - niesu uz nove rozvite uzly => NEUSPECH

    if (this->front->isEmpty() == true) {
        cerr << "AGSearch[" << this->id << "]::step(): NEUSPECH" << endl;
        if (rflag != NULL)
            *rflag = AL::F_NOT_FIND;
        return AL::RET_FAILURE;
    }

    // VYBER 1 UZOL Z FRONTu

    u = this->front->pop();
    assert(u != NULL);
    this->countp++;                     // pocet spracovanych uzlov + 1
    this->mem -= sizeof(Node *);        // memory size - velkost pointra na uzol (1 prvok z frontu frontu)

    cerr << "AGSearch[" << this->id << "]::step(): process: " << AUtils::node2str(u) << endl;

    // CIELOVY TEST

    if (this->goalTest(&u->state) == true) {
        cerr << "AGSearch[" << this->id << "]::step(): >>> !!! MAME CIEL!!! <<< " << endl;

        /*
        // test, docasne - zmazat
        Solution sl;
        Solution isl;
        this->makeSolution(&sl, u);
        this->makeInverseSolution(&isl, u);
        cerr << "AGSearch::stop():  SOLUTION: " << Utils::solution2str(&sl) << endl;
        cerr << "AGSearch::stop(): ISOLUTION: " << Utils::solution2str(&isl) << endl;
        */

        //abort();

        this->fnode = u;

        if (rflag != NULL)
            *rflag = AL::F_THIS_ALGORITMUS;
        return AL::RET_SUCCESS;
        /// \todo zapametaj referenciu na riesenie
    }

    // maximalna hlbka

    if (u->depth == this->maxDepth) {
        /*
        // test, docasne - zmazat
        Solution sl;
        Solution isl;
        this->makeSolution(&sl, u);
        this->makeInverseSolution(&isl, u);
        cerr << "AGSearch::stop():  SOLUTION: " << Utils::solution2str(&sl) << endl;
        cerr << "AGSearch::stop(): ISOLUTION: " << Utils::solution2str(&isl) << endl;
        */

        cerr << "AGSearch[" << this->id << "]::step(): dosiahnuta maximalna hlbka h = " << this->maxDepth << endl;
        /// \todo DELETE_NODE

        if (rflag != NULL)
            *rflag = AL::F_MAX_DEPTH;
        return AL::RET_CONTINUE;
    }

    // maximalne mnozstvo pamete [B]

    if (this->mem >= this->maxMem) {
        cerr << "AGSearch[" << this->id << "]::stop(): vycerpane max. velkost pamete, this.mem=" << this->mem << " B, this.maxMem=" << this->maxMem << " B" << endl;
        /// \todo DELETE_NODE
        if (rflag != NULL)
            *rflag = AL::F_MAX_MEM;
        return AL::RET_FAILURE;
    }

    // ROZVI UZOL - rozvi stavy, vytvor novy uzol pre kazdy stav, uloz na front

    FlowerStates fu;                                        // zoznam rozvitych uzlov

    this->gm->setState(&u->state);
    this->gm->flower(&fu, this->gm->getInvOpr(u->op));      // ! pozor na inverzny operator

    // ziaden rozvity uzol
    if (fu.size() == 0) {
        cerr << "AGSearch[" << this->id << "]::step(): WARNING: ziaden rozvity uzol!" << endl;
        // delete u
        if (rflag != NULL)
            *rflag = AL::F_NONE;
        return AL::RET_CONTINUE;
    }

    FlowerState ftmp;       // novo rozvite stavy
    Node *utmp = NULL;      // ukazuje na novo vytvoreny uzol z novo rozviteho stavu

    // vytvor nove uzly z novo rozvitych stavov
    // pre kazdy stav:

    // for (unsigned int i = 0; i < fu.size(); i++) {   !!! ZASADNA CHYBA !!!
    while (fu.empty() != true) {

        // vyber prvok zo zoznamu FlowerState := {operator, stav}

        ftmp = fu.front();
        fu.pop_front();

        // vytvor novy uzol
        utmp = AUtils::createNode(this->nextId++, u->depth+1, 0, u, &ftmp.s, ftmp.op);
        if (utmp == NULL) {
            cerr << "ERROR: AGSearch[" << this->id << "]::step(): createNode() return NULL (allocation problem)" << endl;
            continue;
        }

        // zvys pocet referencii rodicovskeho uzla
        u->ref += 1;

        // uloz novy uzol na front
        this->front->push(utmp);

        // zvys pocet rozvitych uzlov
        this->countf += 1;

        // zvys obsadenu kapacitu pamete

        this->mem += sizeof(Node *);
        this->mem += sizeof(Node);
        this->mem += this->gm->getStateMemSize();

        cerr << "AGSearch[" << this->id << "]::step(): new: " << AUtils::node2str(utmp) << endl;

        /*
        // test, docasne - zmazat
        Solution sl;
        Solution isl;
        this->makeSolution(&sl, utmp);
        this->makeInverseSolution(&isl, utmp);
        cerr << "AGSearch::stop():  SOLUTION: " << Utils::solution2str(&sl) << endl;
        cerr << "AGSearch::stop(): ISOLUTION: " << Utils::solution2str(&isl) << endl;
        */
    }

    cerr << "AGSearch[" << this->id << "]::step(): RESULT: rozvite: " << this->countf << "; spracovane: " << this->countp << "; pouzita pamet: " << this->mem << " B" << endl;

    if (rflag != NULL)
        *rflag = AL::F_NONE;
    return AL::RET_CONTINUE;
}
