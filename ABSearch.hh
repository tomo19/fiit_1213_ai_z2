#ifndef BSEARCH_HH
#define BSEARCH_HH

#include "AL.hh"
#include "AGSearching.hh"
#include "IDataStore.hh"
#include "State.hh"
#include <cstdint>

/// Obosjmerne prehladavanie

class ABSearch
{
private:
    State sstate;               ///< zaciatocny stav
    State fstate;               ///< cielovy stav

    enum AL::AType      a1type;
    uint16_t            a1maxDepth;
    int                 a1maxMem;
    enum AL::ARetStatus a1ret;
    unsigned int        a1runs;
    enum AL::AType      a2type;
    uint16_t            a2maxDepth;
    int                 a2maxMem;
    enum AL::ARetStatus a2ret;
    unsigned int        a2runs;

    unsigned int countp;
    unsigned int countf;

    AGSearch *a1;
    AGSearch *a2;
    IDataStore *f1;
    IDataStore *f2;

public:
    ABSearch(uint8_t M, uint8_t N, State sstate, State fstate, enum AL::AType a1type, uint16_t a1maxDepth, int a1maxMem, unsigned int a1runs, enum AL::AType a2type, uint16_t a2maxDepth, int a2maxMem, unsigned int a2runs);
    /// \todo
    ~ABSearch();
    enum AL::ARetStatus step(enum AL::ARetFlag *const rflag = NULL);
};

#endif // BSEARCH_HH
