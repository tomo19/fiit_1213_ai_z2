#ifndef SOLUTION_HH
#define SOLUTION_HH

#include "State.hh"
#include "GamePlay_common.hh"
#include <list>

using namespace std;

/// Riesenie prolemu

///
/// - zaciatocny stav
/// - postupnost operatorov veducich k cielovemu stavu

struct Solution
{
    State sstate;                   ///< zaciatocny stav
    list<Operator> ops;             ///< postupnost operatorov veducich k cielovemu stavu
};

#endif // SOLUTION_HH
