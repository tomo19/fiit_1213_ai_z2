#ifndef TDD_HH
#define TDD_HH

#include "FS.hh"
#include <string>

using namespace std;

// neuspesne testy koncia abort()

class TDD
{
private:
    static void prtHeader(string head);

public:
    static void testAll();
    static void testFIFO();
    static void testGPConstructorAndOutputs();
    static void testGPemptyBox();
    static void testGPflower();
    static void testGPgetSize();
    static void testGPops();
    static void testGPsetStav();
    static void testGPswap();
    static void testLIFO();

    /// \todo dat do private ak to nebude treba volat inde
    static void test_prtFSS(int M, int N, const FlowerStates *const f);

};

#endif // TDD_HH
