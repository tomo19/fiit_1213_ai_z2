#ifndef FIFO_HH
#define FIFO_HH

#include "IDataStore.hh"
#include "Node.hh"
#include <queue>

class FIFO : public IDataStore
{
private:
    queue<Node *> fifo;

public:
    FIFO();    
    bool isEmpty();
    Node* pop();
    void push(Node *node);
};

#endif // FIFO_HH
