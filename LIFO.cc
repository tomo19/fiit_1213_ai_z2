#include "LIFO.hh"
#include "Node.hh"
#include <cassert>

LIFO::LIFO()
{
}

// true - nula, else false

bool LIFO::isEmpty()
{
    return this->lifo.empty();
}

// size decreased 1
// top element removed
// return value: reference

Node * LIFO::pop()
{
    assert(this->lifo.empty() == false);

    Node *tmp = NULL;

    tmp = this->lifo.top();
    this->lifo.pop();

    return tmp;
}

void LIFO::push(Node *node)
{
    assert(node != NULL);

    this->lifo.push(node);
}
